<?php
/**
 * Created by PhpStorm.
 * User: hailanzhou
 * Date: 2021-03-05
 * Time: 13:36
 */

namespace NebulaTool\Elasticsearch;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class Elasticsearch
{

    /**
     * @param array                $config
     *
     * @return Builder
     */
    public static function builder(array $config): Builder
    {
        return new Builder(
            new Query(new Grammar(), static::clientBuilder($config))
        );
    }

    /**
     * @param array                $config
     *
     * @return Client
     */
    protected static function clientBuilder(array $config): Client
    {
        $clientBuilder = ClientBuilder::create();

        $clientBuilder
            ->setConnectionPool($config['connection_pool'])
            ->setSelector($config['selector'])
            ->setHosts($config['hosts']);
        return $clientBuilder->build();
    }
}
